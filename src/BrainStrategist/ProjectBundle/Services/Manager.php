<?php
namespace BrainStrategist\ProjectBundle\Services;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\RequestStack;
use BrainStrategist\ProjectBundle\Entity\Project;
use Doctrine\ORM\EntityManager;

/**
 * Class Manager
 * @package BrainStrategist\ProjectBundle\Services
 */
class Manager
{
    protected $em;
    protected $requestStack;

    /**
     * Manager constructor.
     * @param EntityManager $em
     * @param RequestStack $requestStack
     * @param PaginatorAware $paginator
     */
    public function __construct(EntityManager $em, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    /**
     * @param string $mode
     * @return array
     */
    public function getTickets($mode = "kanban")
    {
        $tickets = array();
        $tickets['viewtype'] = $mode;

        $status = array();
        $tickets_list = array();

        $ticketsEntity = $this->em->getRepository("BrainStrategistProjectBundle:Ticket");
        $statusEntity = $this->em->getRepository("BrainStrategistProjectBundle:Ticket_status");

        switch ($mode) {
            case 'kanban':
                $params['kanban'] = true;

                /**
                 * Kanban Mode
                 */
                $ticket_query = $ticketsEntity->findAllTicketByUserQuery($params);
                $ticket_query->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                $ticket_results = $ticket_query->getArrayResult();

                // Get all status by project
                $status_results = $statusEntity->findAllProjectStatusByUserID($this->currentUser->getId());

                foreach ($status_results as $status_row) {
                    $status[$status_row['project']['name']][$status_row["name"]] = $status_row;
                }

                foreach ($ticket_results as $ticket) {
                    $tickets_list[$ticket['projet']['name']][$ticket["status"]["name"]]['status_id'] = $ticket["status"]['id'];
                    $tickets_list[$ticket['projet']['name']][$ticket["status"]["name"]]['tickets'][] = $ticket;
                }
                /**
                 *  check if a status haven't any tickets and add a blank column in the kanban
                 */
                foreach ($status as $key => $project) {
                    foreach ($project as $key2 => $status_project) {
                        if (!isset($tickets_list[$key][$key2])) {
                            $tickets_list[$key][$key2]['status_id'] = $status_project['id'];
                        }
                    }
                }

                $tickets['total_tickets'] = sizeof($ticket_results);

                break;
            default:
                $request = $this->requestStack->getCurrentRequest();

                /**
                 * Classical listing view
                 */
                if (null !== $request->query->getInt('page')) $page = 1;

                if (null !== $request->query->getInt('limit')) $limit = 10;

                $ticket_query = $ticketsEntity->findAllTicketByUserQuery($params);
                $pagination = $this->container->get("pagination");
                $tickets_list = $pagination->paginate(
                    $ticket_query,
                    $page,
                    $limit
                );
                break;

        }
        $tickets['tickets'] = $tickets_list;
        return $tickets;
    }

    /**
     * @param Project|null $project
     * @return string
     */
    public function assignPictureProject( Project $project)
    {

        $file = $project->getPicture();

        if(!$project instanceof Project){
            throw new Exception('$project must be an instance of BrainStrategist\ProjectBundle\Entity\Project ');
        }
        if(!is_null($project->getPicture()) && $project->getPicture()!="" ) {
            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                $this->container->getParameter('full_project_directory'),
                $fileName
            );
            // Update the 'brochure' property to store the PDF file name
            // instead of its contents
            $project->setPicture($fileName);
        }
        return $project->getPicture();
    }

    /**
     * @param Project $project
     * @param $slug
     * @param $currentUser
     */
    public function initProject( Project $project, $slug, $currentUser){

        $organizationEntity= $this->em->getRepository("BrainStrategistKernelBundle:Organization");

        if(!$project instanceof Project){
            throw new Exception('$project must be an instance of BrainStrategist\ProjectBundle\Entity\Project ');
        }
        $organization = $organizationEntity->findOneBySlug($slug);
        // when the user create the organization, i add himself into the organization.
        $project->addUsersProject($currentUser);
        $project->setCreator($currentUser);
        $project->setOrganization($organization);
        $organization->addProjectsOrganization($project);
        $currentUser->addProject($project);

        $this->em->persist($project);
        $this->em->persist($organization);
        $this->em->persist($currentUser);
        $this->em->flush();
    }

    public function getOwnerProject($id,$currentUser){
        $projectEntity = $this->em->getRepository("BrainStrategistProjectBundle:Project");
        $project = $projectEntity->find($id);
        if($projectEntity->isMyProject($id,$currentUser->getId()) && $project->getCreator()->getId()==$currentUser->getId()){
            return $project;
        }
        return null;
    }

    public function saveProject(Project $project,$response){

        if(!$project instanceof Project){
            throw new Exception('$project must be an instance of BrainStrategist\ProjectBundle\Entity\Project ');
        }

        $this->em->persist($project);
        $this->em->persist($response);
        $this->em->flush();

        return $project;
    }

}