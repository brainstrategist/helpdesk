<?php
namespace BrainStrategist\ProjectBundle\Services;

use BrainStrategist\ProjectBundle\Entity\Project;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Class Breadcrumbs
 * @package BrainStrategist\ProjectBundle\Services
 */
class Breadcrumbs
{
    protected $breadcrumbs;
    protected $requestStack;

    public function __construct(RequestStack $requestStack)
    {

        $this->requestStack = $requestStack;
        $this->breadcrumbs = $this->get("white_october_breadcrumbs");
    }

    public function severityManage($slug){
        $this->breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("kernel"));
        $this->breadcrumbs->addItem($this->get('translator')->trans("Organizations"), $this->get("router")->generate("kernel"));
        $this->breadcrumbs->addItem($this->get('translator')->trans("Projects"), $this->get("router")->generate("organize_access", array("slug" => $slug)));
        return $this->breadcrumbs;
    }

    public function severityList($slug,Project $project){

        if(!$project instanceof Project){
            throw new Exception('$project must be an instance of BrainStrategist\ProjectBundle\Entity\Project ');
        }
        $this->breadcrumbs->addItem($this->get('translator')->trans("Projects"), $this->get("router")->generate("organize_access", array("slug" => $slug)));
        $this->breadcrumbs->addItem($project->getName(), $this->get("router")->generate("project_access", array("slug" => $slug)));
        $this->breadcrumbs->addItem($this->get('translator')->trans("Severities"), $this->get("router")->generate("severity_list", array("slug" => $slug)));
        return $this->breadcrumbs;
    }
}
?>