<?php

namespace BrainStrategist\ProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;

use BrainStrategist\ProjectBundle\Entity\Project;
use BrainStrategist\ProjectBundle\Form\ProjectForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RequestStack;

class ProjectController extends Controller
{

    private $currentUser;

    /**
     *
     * Pre dispatcher event to check the security access of the current user
     *
     */
    private function preExecute()
    {

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->currentUser = $this->get('security.token_storage')->getToken()->getUser();
        } else {
            throw new HttpException(400, "You are not allowed to access Project. Please register or login first");
        }
    }


    /**
     * @param RequestStack $request
     * @param null $id
     * @param null $slug
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/{_locale}/organization/{slug}/project/create",name="project_create")
     * @Route("/{_locale}/organization/{slug}/project/edit/{id}",name="project_edit")
     */
    public function manageAction(RequestStack $request, $id = null, $slug = null)
    {

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("kernel"));
        $breadcrumbs->addItem($this->get('translator')->trans("Organizations"), $this->get("router")->generate("kernel"));
        $breadcrumbs->addItem($this->get('translator')->trans("Projects"), $this->get("router")->generate("organize_access", array("slug" => $slug)));

        $params = array();

        if (isset($id) && isset($slug)) {
            $breadcrumbs->addItem($this->get('translator')->trans("Edit"));

            $project = $this->get("brain_strategist_project.manager")->getOwnerProject($id,$this->currentUser);
            if (is_null($project)) return $this->redirectToRoute("default");

            if (!is_null($project->getPicture()) && $project->getPicture() != "") {
                $params['picture'] = $project->getPicture();
                $project->setPicture(
                    new File($this->getParameter('full_project_directory') . '/' . $project->getPicture())
                );
            }

        } else {
            $project = new Project();
        }

        $form = $this->createForm(ProjectForm::class, $project);

        if ('POST' === $request->getMethod() && $form->isSubmitted() && $form->isValid()) {

            $form->handleRequest($request);
            $response = $form->getData();

            $this->get("brain_strategist_project.manager")->assignPictureProject($project);
            $this->get("brain_strategist_project.manager")->saveProject($project,$response);

            if (is_null($id)) $this->get("brain_strategist_project.manager")->initProject($project, $slug, $this->currentUser);

            return $this->redirectToRoute("organize_access", array('slug' => $slug));

        }
        $params = array_merge($params, array("form" => $form->createView()));
        return $this->render(
            'BrainStrategistProjectBundle:Project:manage.html.twig',
            $params
        );
    }

    /**
     * @Route("/{_locale}/project/{slug}/dashboard",name="project_access")
     * @Route("/{_locale}/project/{slug}/view/{view}",name="project_view")
     */
    public function accessAction(Request $request, $slug = null, $view = null)
    {

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("kernel"));
        $breadcrumbs->addItem($this->get('translator')->trans("Organizations"), $this->get("router")->generate("kernel"));
        $breadcrumbs->addItem($this->get('translator')->trans("Projects"), $this->get("router")->generate("organize_access", array("slug" => $slug)));

        $params = array();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();

        $projectEntity = $em->getRepository("BrainStrategistProjectBundle:Project");

        if (isset($slug)) {

            if (!$this->currentUser) {
                $this->currentUser = $this->get('security.token_storage')->getToken()->getUser();
            }
            if ($projectEntity->isMyProject($slug, $this->currentUser->getId())) {
                $project = $projectEntity->findOneBySlug($slug);
                $params['project'] = $project;
                $organizationEntity = $em->getRepository("BrainStrategistKernelBundle:Organization");
                $organization = $organizationEntity->find($project->getOrganization()->getId());
                $params['organization'] = $organization;

                $notice = $request->attributes->get('notice');
                $type_notice = $request->attributes->get('type_notice');
                if (isset($notice)) {
                    $params['notice'] = $notice;
                    $params['type_notice'] = $type_notice;
                }

                if (isset($view)) {
                    $params['view'] = $this->renderView(
                        'BrainStrategistProjectBundle:Project:parts/' . $view . '.html.twig',
                        $params
                    );
                }
            }
        }


        return $this->render(
            'BrainStrategistProjectBundle:Project:overview.html.twig',
            $params
        );
    }
}