<?php

namespace BrainStrategist\ProjectBundle\Controller;

use BrainStrategist\ProjectBundle\Entity\Severity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\HttpException;

use BrainStrategist\ProjectBundle\Form\SeverityForm;


class ProjectSeverityController extends Controller
{

    private $currentUser;
    private $breadcrumbs;

    /**
     *
     * Pre dispatcher event to check the security access of the current user
     *
     */
    private function preExecute()
    {

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->currentUser = $this->get('security.token_storage')->getToken()->getUser();


        } else throw new HttpException(400, "You are not allowed to access Project. Please register or login first");

    }


    /**
     * @Route("/{_locale}/project/{slug}/severity/create",name="severity_create")
     * @Route("/{_locale}/project/{slug}/severity/edit/{id}",name="severity_edit")
     */
    public function manageAction(Request $request, $id = null, $slug = null)
    {

        $breadcrumbs = $this->get("brain_strategist_project.breadcrumbs")->severityManage($slug);

        $params = array();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $projectEntity = $em->getRepository("BrainStrategistProjectBundle:Project");
        $severityEntity = $em->getRepository("BrainStrategistProjectBundle:Severity");
        $severity = new Severity();

        if (isset($slug))
            $project = $projectEntity->findBySlug($slug);
        else
            return $this->redirectToRoute("default");

        if (isset($id)) {
            $breadcrumbs->addItem($this->get('translator')->trans("Edit"));

            if ($projectEntity->isMyProject($project[0]->getId(), $this->currentUser->getId()) && $project[0]->getCreator()->getId() == $this->currentUser->getId()) {
                $severity = $severityEntity->find($id);
            } else
                return $this->redirectToRoute("default");
        }
        $form = $this->createForm(SeverityForm::class, $severity);

        if ('POST' == $request->getMethod() && $form->isSubmitted() && $form->isValid()) {

            $form->handleRequest($request);
            $severity->setProject($project[0]);
            $response = $form->getData();
            if (is_null($id)) $em->persist($severity);
            $em->persist($response);
            $em->flush();

            return $this->redirectToRoute("severity_list", array('slug' => $slug));

        }
        $params = array_merge($params, array("form" => $form->createView()));
        return $this->render(
            'BrainStrategistProjectBundle:Severity:manage.html.twig',
            $params
        );

    }

    /**
     * @Route("/{_locale}/project/{slug}/severity/list",name="severity_list")
     */
    public function listAction(Request $request, $slug = null)
    {
        $this->preExecute();

        $params = array();
        $em = $this->getDoctrine()->getManager();
        $projectEntity = $em->getRepository("BrainStrategistProjectBundle:Project");

        if (isset($slug)) {
            if ($projectEntity->isMyProject($slug, $this->currentUser->getId())) {

                $project = $projectEntity->findOneBySlug($slug);

                // Get all severity by project
                $severityEntity = $em->getRepository("BrainStrategistProjectBundle:Severity");
                $severity = $severityEntity->findAllByProjectId($project->getId());
                $severityQuery = $severity->getQuery();
                $severityQuery->setHydrationMode(\Doctrine\ORM\Query::HYDRATE_ARRAY);

                $params = array("projectID" => $project->getId(), "severities" => $severityQuery->getArrayResult(), "slug" => $slug, "projet" => $project);
            }
            $this->breadcrumbs = $this->get("brain_strategist_project.breadcrumbs")->severityList($project, $slug);
        }
        return $this->render(
            'BrainStrategistProjectBundle:Severity:list.html.twig',
            $params
        );
    }
}