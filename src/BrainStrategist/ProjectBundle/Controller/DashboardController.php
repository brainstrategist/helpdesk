<?php

namespace BrainStrategist\ProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DashboardController extends Controller
{
    private $currentUser;


    /**
     * @Route("/{_locale}/user/dashboard",name="dashboard_access")
     * @Route("/{_locale}/user/dashboard/{viewtype}",name="dashboard_kanban")
     */
    public function accessAction(Request $request, $viewType = null)
    {

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans("Home"), $this->get("router")->generate("kernel"));
        $breadcrumbs->addItem($this->get('translator')->trans("Organizations"), $this->get("router")->generate("kernel"));
        $breadcrumbs->addItem($this->get('translator')->trans("Dashboard"), $this->get("router")->generate("dashboard_access"));

        $params = array(
            "userID" => $this->currentUser->getId(),
            "limit" => 100,
            "offset" => 0);

        $params = array_merge($params, $this->get("brain_strategist_project.manager")->getTickets($viewType));
        return $this->render(
            'BrainStrategistProjectBundle:Dashboard:overview.html.twig',
            $params
        );
    }
}
